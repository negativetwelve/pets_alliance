# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120626075117) do

  create_table "battles", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pet_id"
    t.integer  "opponent_id"
    t.text     "log"
    t.boolean  "in_progress"
    t.integer  "turn_number"
    t.boolean  "first"
  end

  create_table "categories", :force => true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.integer  "pid"
    t.text     "description"
  end

  create_table "friendships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  create_table "items", :force => true do |t|
    t.string   "name"
    t.integer  "unlock_level"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pid"
    t.text     "description"
    t.text     "power_ups"
    t.boolean  "battle_useable"
    t.boolean  "sellable"
    t.text     "attributes_list"
    t.integer  "level"
    t.text     "item_type"
    t.boolean  "can_equip"
    t.text     "cost"
  end

  create_table "items_pets", :id => false, :force => true do |t|
    t.integer "item_id"
    t.integer "pet_id"
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "pid"
    t.text     "neighbors"
    t.text     "places"
  end

  create_table "opponents", :force => true do |t|
    t.string   "name"
    t.integer  "opp_id"
    t.integer  "attack"
    t.integer  "defense"
    t.integer  "hp"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "pet_id"
    t.text     "equipment"
    t.integer  "alliance_count"
  end

  create_table "pet_stats", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "attack"
  end

  create_table "pets", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pet_type"
    t.integer  "win"
    t.integer  "loss"
    t.boolean  "current"
    t.boolean  "in_battle"
    t.integer  "opponent_id"
    t.text     "items_list"
    t.text     "equipment"
    t.integer  "idle_wins"
    t.integer  "idle_losses"
    t.text     "money"
    t.integer  "runs"
    t.text     "petstats"
    t.text     "location_history"
    t.text     "log"
    t.text     "toggles"
  end

  create_table "petstats", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "stat_type"
  end

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "topic_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quests", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.text     "action"
    t.text     "requirements"
    t.integer  "pid"
  end

  create_table "subcategories", :force => true do |t|
    t.string   "title"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topics", :force => true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.text     "views"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password"
    t.string   "salt"
    t.boolean  "admin",                :default => false
    t.string   "alliance_code"
    t.integer  "forum_posts_per_page"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

end
