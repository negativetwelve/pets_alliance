class RemovePetDescriptionFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :description
  end

  def self.down
    add_column :pets, :description, :text
  end
end
