class AddMoneyToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :money, :integer
  end

  def self.down
    remove_column :pets, :money
  end
end
