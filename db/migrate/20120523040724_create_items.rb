class CreateItems < ActiveRecord::Migration
  def self.up
    create_table :items do |t|
      t.string :name
      t.string :item_type
      t.integer :unlock_level
      t.integer :cost
      t.integer :attack
      t.integer :defense
      t.integer :hp
      t.integer :energy
      t.integer :money
      t.references :pet

      t.timestamps
    end
  end

  def self.down
    drop_table :items
  end
end
