class RemoveItemsListFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :items_list
  end

  def self.down
    add_column :pets, :items_list, :binary
  end
end
