class RemoveItemssFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :items
  end

  def self.down
    add_column :pets, :items, :text
  end
end
