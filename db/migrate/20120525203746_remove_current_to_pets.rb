class RemoveCurrentToPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :active
  end

  def self.down
    add_column :pets, :active, :boolean
  end
end
