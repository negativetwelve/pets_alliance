class RemoveSubcategoriesIdFromTopics < ActiveRecord::Migration
  def self.up
    remove_column :topics, :subcategory_id
  end

  def self.down
    add_column :topics, :subcategory_id, :integer
  end
end
