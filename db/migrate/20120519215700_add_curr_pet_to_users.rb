class AddCurrPetToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :curr_pet, :string
  end

  def self.down
    remove_column :users, :curr_pet
  end
end
