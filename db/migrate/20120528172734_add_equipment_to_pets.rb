class AddEquipmentToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :equipment, :text
  end

  def self.down
    remove_column :pets, :equipment
  end
end
