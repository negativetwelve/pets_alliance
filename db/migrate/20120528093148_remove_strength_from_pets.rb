class RemoveStrengthFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :strength
    remove_column :pets, :strength_lvl
    remove_column :pets, :strength_exp
    remove_column :pets, :attack_lvl
    remove_column :pets, :attack_exp
    remove_column :pets, :defense_lvl
    remove_column :pets, :defense_exp
    remove_column :pets, :hp_lvl
    remove_column :pets, :hp_exp
  end

  def self.down
    add_column :pets, :hp_exp, :integer
    add_column :pets, :hp_lvl, :integer
    add_column :pets, :defense_exp, :integer
    add_column :pets, :defense_lvl, :integer
    add_column :pets, :attack_exp, :integer
    add_column :pets, :attack_lvl, :integer
    add_column :pets, :strength_exp, :integer
    add_column :pets, :strength_lvl, :integer
    add_column :pets, :strength, :integer
  end
end
