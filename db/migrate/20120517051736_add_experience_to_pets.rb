class AddExperienceToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :experience, :integer
    add_column :pets, :attack_lvl, :integer
    add_column :pets, :defense_lvl, :integer
    add_column :pets, :hp_lvl, :integer
  end

  def self.down
    remove_column :pets, :hp_lvl
    remove_column :pets, :defense_lvl
    remove_column :pets, :attack_lvl
    remove_column :pets, :experience
  end
end
