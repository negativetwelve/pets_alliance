class AddCurrentToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :active, :boolean
  end

  def self.down
    remove_column :pets, :active
  end
end
