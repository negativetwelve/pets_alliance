class AddMoneyTextToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :money, :text
  end

  def self.down
    remove_column :pets, :money
  end
end
