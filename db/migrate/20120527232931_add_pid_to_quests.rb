class AddPidToQuests < ActiveRecord::Migration
  def self.up
    add_column :quests, :pid, :integer
  end

  def self.down
    remove_column :quests, :pid
  end
end
