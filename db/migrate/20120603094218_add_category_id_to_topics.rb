class AddCategoryIdToTopics < ActiveRecord::Migration
  def self.up
    add_column :topics, :category_id, :integer
  end

  def self.down
    remove_column :topics, :category_id
  end
end
