class CreateItemsAndPetsTable < ActiveRecord::Migration
def self.up
  create_table :items_pets, :id => false do |t|
    t.integer :item_id
    t.integer :pet_id
  end
end

def self.down
  drop_table :items_pets
end
end
