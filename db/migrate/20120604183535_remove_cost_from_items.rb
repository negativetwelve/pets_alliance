class RemoveCostFromItems < ActiveRecord::Migration
  def self.up
    remove_column :items, :cost
  end

  def self.down
    add_column :items, :cost, :integer
  end
end
