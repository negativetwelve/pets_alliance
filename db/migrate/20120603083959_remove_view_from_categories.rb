class RemoveViewFromCategories < ActiveRecord::Migration
  def self.up
    remove_column :categories, :views
  end

  def self.down
    add_column :categories, :views, :integer
  end
end
