class AddAttributesListToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :attributes_list, :text
  end

  def self.down
    remove_column :items, :attributes_list
  end
end
