class AddExperiencesToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :experience, :integer
  end

  def self.down
    remove_column :pets, :experience
  end
end
