class RemoveCurrPetFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :curr_pet
  end

  def self.down
    add_column :users, :curr_pet, :string
  end
end
