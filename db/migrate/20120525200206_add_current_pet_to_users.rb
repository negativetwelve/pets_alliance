class AddCurrentPetToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :current_pet, :integer
  end

  def self.down
    remove_column :users, :current_pet
  end
end
