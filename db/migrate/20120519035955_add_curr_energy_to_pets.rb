class AddCurrEnergyToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :curr_energy, :integer
  end

  def self.down
    remove_column :pets, :curr_energy
  end
end
