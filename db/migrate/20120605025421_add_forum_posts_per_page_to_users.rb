class AddForumPostsPerPageToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :forum_posts_per_page, :integer
  end

  def self.down
    remove_column :users, :forum_posts_per_page
  end
end
