class RemoveSpeedAttackLvlFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :speed_attack_lvl
  end

  def self.down
    add_column :pets, :speed_attack_lvl, :integer
  end
end
