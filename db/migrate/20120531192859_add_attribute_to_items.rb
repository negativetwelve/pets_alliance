class AddAttributeToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :attributes, :text
    add_column :items, :power_ups, :text
    add_column :items, :battle_useable, :boolean
    add_column :items, :sellable, :boolean
  end

  def self.down
    remove_column :items, :sellable
    remove_column :items, :battle_useable
    remove_column :items, :power_ups
    remove_column :items, :attributes
  end
end
