class RemoveItemFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :item
  end

  def self.down
    add_column :pets, :item, :references
  end
end
