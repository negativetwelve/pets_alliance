class AddActionToQuests < ActiveRecord::Migration
  def self.up
    add_column :quests, :action, :text
  end

  def self.down
    remove_column :quests, :action
  end
end
