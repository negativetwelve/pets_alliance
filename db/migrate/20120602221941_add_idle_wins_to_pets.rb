class AddIdleWinsToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :idle_wins, :integer
    add_column :pets, :idle_losses, :integer
  end

  def self.down
    remove_column :pets, :idle_losses
    remove_column :pets, :idle_wins
  end
end
