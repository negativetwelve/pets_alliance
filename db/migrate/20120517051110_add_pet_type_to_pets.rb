class AddPetTypeToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :type, :string
  end

  def self.down
    remove_column :pets, :type
  end
end
