class AddItemsListToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :items_list, :binary
  end

  def self.down
    remove_column :pets, :items_list
  end
end
