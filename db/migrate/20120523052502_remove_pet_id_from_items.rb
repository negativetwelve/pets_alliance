class RemovePetIdFromItems < ActiveRecord::Migration
  def self.up
    remove_column :items, :pet_id
  end

  def self.down
    add_column :items, :pet_id, :integer
  end
end
