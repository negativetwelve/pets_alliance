class AddItemPointsToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :item_points, :integer
  end

  def self.down
    remove_column :pets, :item_points
  end
end
