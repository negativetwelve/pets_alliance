class AddQuestExpToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :quest_exp, :integer
  end

  def self.down
    remove_column :pets, :quest_exp
  end
end
