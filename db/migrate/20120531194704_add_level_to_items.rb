class AddLevelToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :level, :integer
  end

  def self.down
    remove_column :items, :level
  end
end
