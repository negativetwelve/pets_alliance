class RemoveItemSkillPointsFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :item_skill_points
  end

  def self.down
    add_column :pets, :item_skill_points, :integer
  end
end
