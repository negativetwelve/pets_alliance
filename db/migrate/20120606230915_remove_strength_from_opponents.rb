class RemoveStrengthFromOpponents < ActiveRecord::Migration
  def up
    remove_column :opponents, :strength
        remove_column :opponents, :speed_attack
      end

  def down
    add_column :opponents, :speed_attack, :integer
    add_column :opponents, :strength, :integer
  end
end
