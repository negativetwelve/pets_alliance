class RemoveCategoryPidFromTopics < ActiveRecord::Migration
  def self.up
    remove_column :topics, :category_pid
  end

  def self.down
    add_column :topics, :category_pid, :integer
  end
end
