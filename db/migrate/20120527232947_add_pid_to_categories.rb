class AddPidToCategories < ActiveRecord::Migration
  def self.up
    add_column :categories, :pid, :integer
  end

  def self.down
    remove_column :categories, :pid
  end
end
