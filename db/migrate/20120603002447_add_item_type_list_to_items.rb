class AddItemTypeListToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :item_type, :text
  end

  def self.down
    remove_column :items, :item_type
  end
end
