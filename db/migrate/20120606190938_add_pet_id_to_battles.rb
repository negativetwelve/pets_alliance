class AddPetIdToBattles < ActiveRecord::Migration
  def change
    add_column :battles, :pet_id, :integer

    add_column :battles, :opponent_id, :integer

  end
end
