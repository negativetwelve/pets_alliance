class AddRequirementsToQuests < ActiveRecord::Migration
  def self.up
    add_column :quests, :requirements, :text
  end

  def self.down
    remove_column :quests, :requirements
  end
end
