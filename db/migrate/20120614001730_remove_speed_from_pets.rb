class RemoveSpeedFromPets < ActiveRecord::Migration
  def up
    remove_column :pets, :speed
      end

  def down
    add_column :pets, :speed, :integer
  end
end
