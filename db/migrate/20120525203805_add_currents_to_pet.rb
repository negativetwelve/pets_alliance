class AddCurrentsToPet < ActiveRecord::Migration
  def self.up
    add_column :pets, :current, :boolean
  end

  def self.down
    remove_column :pets, :current
  end
end
