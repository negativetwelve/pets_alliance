class RemoveQuestExpFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :quest_lvl
    remove_column :pets, :quest_exp
  end

  def self.down
    add_column :pets, :quest_exp, :integer
    add_column :pets, :quest_lvl, :integer
  end
end
