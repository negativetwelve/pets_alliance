class RemoveCurrentPetFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :current_pet
  end

  def self.down
    add_column :users, :current_pet, :integer
  end
end
