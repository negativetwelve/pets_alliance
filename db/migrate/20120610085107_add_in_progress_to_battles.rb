class AddInProgressToBattles < ActiveRecord::Migration
  def change
    add_column :battles, :in_progress, :boolean

  end
end
