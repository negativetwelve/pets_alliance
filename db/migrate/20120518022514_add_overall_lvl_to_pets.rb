class AddOverallLvlToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :overall_lvl, :integer
  end

  def self.down
    remove_column :pets, :overall_lvl
  end
end
