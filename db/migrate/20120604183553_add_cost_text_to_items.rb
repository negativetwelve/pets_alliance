class AddCostTextToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :cost, :text
  end

  def self.down
    remove_column :items, :cost
  end
end
