class RemovePetTypesFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :pet_type
  end

  def self.down
    add_column :pets, :pet_type, :string
  end
end
