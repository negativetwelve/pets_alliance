class AddItemsListsToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :items_list, :text
  end

  def self.down
    remove_column :pets, :items_list
  end
end
