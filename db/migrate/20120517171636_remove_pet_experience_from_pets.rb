class RemovePetExperienceFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :overall_lvl
    remove_column :pets, :experience
    remove_column :pets, :attack_lvl
    remove_column :pets, :defense_lvl
    remove_column :pets, :hp_lvl
  end

  def self.down
    add_column :pets, :hp_lvl, :integer
    add_column :pets, :defense_lvl, :integer
    add_column :pets, :attack_lvl, :integer
    add_column :pets, :experience, :integer
    add_column :pets, :overall_lvl, :integer
  end
end
