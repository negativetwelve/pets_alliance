class AddOpponentIdToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :opponent_id, :integer
  end

  def self.down
    remove_column :pets, :opponent_id
  end
end
