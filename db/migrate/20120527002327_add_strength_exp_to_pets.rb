class AddStrengthExpToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :strength_exp, :integer
    add_column :pets, :speed_attack_exp, :integer
    add_column :pets, :strength_lvl, :integer
    add_column :pets, :speed_attack_lvl, :integer
  end

  def self.down
    remove_column :pets, :speed_attack_lvl
    remove_column :pets, :strength_lvl
    remove_column :pets, :speed_attack_exp
    remove_column :pets, :strength_exp
  end
end
