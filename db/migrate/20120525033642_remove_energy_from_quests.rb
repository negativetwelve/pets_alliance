class RemoveEnergyFromQuests < ActiveRecord::Migration
  def self.up
    remove_column :quests, :energy
    remove_column :quests, :money
  end

  def self.down
    add_column :quests, :money, :integer
    add_column :quests, :energy, :integer
  end
end
