class RemoveDescriptionFromTopics < ActiveRecord::Migration
  def self.up
    remove_column :topics, :description
  end

  def self.down
    add_column :topics, :description, :text
  end
end
