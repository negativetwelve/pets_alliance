class AddStrengthToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :strength, :integer
    add_column :pets, :speed_attack, :integer
  end

  def self.down
    remove_column :pets, :speed_attack
    remove_column :pets, :strength
  end
end
