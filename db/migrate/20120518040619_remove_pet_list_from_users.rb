class RemovePetListFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :petlist
  end

  def self.down
    add_column :users, :petlist, :string
  end
end
