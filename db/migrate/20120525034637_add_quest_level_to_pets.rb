class AddQuestLevelToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :quest_lvl, :integer
    add_column :pets, :win, :integer
    add_column :pets, :loss, :integer
  end

  def self.down
    remove_column :pets, :loss
    remove_column :pets, :win
    remove_column :pets, :quest_lvl
  end
end
