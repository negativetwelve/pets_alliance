class RemoveAttackSpeedFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :attack_speed
    remove_column :pets, :attack_speed_exp
    remove_column :pets, :attack_speed_lvl
  end

  def self.down
    add_column :pets, :attack_speed_lvl, :integer
    add_column :pets, :attack_speed_exp, :integer
    add_column :pets, :attack_speed, :integer
  end
end
