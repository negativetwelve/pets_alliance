class RemoveActionFromQuests < ActiveRecord::Migration
  def self.up
    remove_column :quests, :action
  end

  def self.down
    add_column :quests, :action, :text
  end
end
