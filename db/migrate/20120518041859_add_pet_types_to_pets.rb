class AddPetTypesToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :pet_type, :string
  end

  def self.down
    remove_column :pets, :pet_type
  end
end
