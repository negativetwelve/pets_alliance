class AddInBattleToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :in_battle, :boolean
  end

  def self.down
    remove_column :pets, :in_battle
  end
end
