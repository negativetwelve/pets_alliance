class RemoveSpeedAttackExpFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :speed_attack_exp
    remove_column :pets, :speed_attack
    remove_column :pets, :speed_attack_level
  end

  def self.down
    add_column :pets, :speed_attack_level, :integer
    add_column :pets, :speed_attack, :integer
    add_column :pets, :speed_attack_exp, :integer
  end
end
