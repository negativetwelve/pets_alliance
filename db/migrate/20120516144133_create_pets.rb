class CreatePets < ActiveRecord::Migration
  def self.up
    create_table :pets do |t|
      t.string :name
      t.string :pet_type
      t.references :user
      t.text :description
      t.integer :overall_lvl

      t.timestamps
    end
  end

  def self.down
    drop_table :pets
  end
end
