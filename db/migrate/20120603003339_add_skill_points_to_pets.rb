class AddSkillPointsToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :skill_points, :integer
  end

  def self.down
    remove_column :pets, :skill_points
  end
end
