class AddCanEquipToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :can_equip, :boolean
  end

  def self.down
    remove_column :items, :can_equip
  end
end
