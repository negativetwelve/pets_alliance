class RemoveCurrentPetFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :current_pet
  end

  def self.down
    add_column :pets, :current_pet, :integer
  end
end
