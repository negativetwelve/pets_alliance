class RemoveStatsFromPets < ActiveRecord::Migration
  def up
    remove_column :pets, :overall_lvl
        remove_column :pets, :mining_lvl
        remove_column :pets, :woodcutting_lvl
        remove_column :pets, :max_energy
        remove_column :pets, :curr_energy
        remove_column :pets, :defense
        remove_column :pets, :curr_hp
        remove_column :pets, :max_hp
        remove_column :pets, :mining_exp
        remove_column :pets, :woodcuting_exp
        remove_column :pets, :experience
        remove_column :pets, :skill_points
        remove_column :pets, :item_points
      end

  def down
    add_column :pets, :item_points, :integer
    add_column :pets, :skill_points, :integer
    add_column :pets, :experience, :integer
    add_column :pets, :woodcuting_exp, :integer
    add_column :pets, :mining_exp, :integer
    add_column :pets, :max_hp, :integer
    add_column :pets, :curr_hp, :integer
    add_column :pets, :defense, :integer
    add_column :pets, :curr_energy, :integer
    add_column :pets, :max_energy, :integer
    add_column :pets, :woodcutting_lvl, :integer
    add_column :pets, :mining_lvl, :integer
    add_column :pets, :overall_lvl, :integer
  end
end
