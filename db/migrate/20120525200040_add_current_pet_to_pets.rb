class AddCurrentPetToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :current_pet, :integer
  end

  def self.down
    remove_column :pets, :current_pet
  end
end
