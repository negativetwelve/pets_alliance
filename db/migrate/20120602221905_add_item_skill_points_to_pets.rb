class AddItemSkillPointsToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :item_skill_points, :integer
  end

  def self.down
    remove_column :pets, :item_skill_points
  end
end
