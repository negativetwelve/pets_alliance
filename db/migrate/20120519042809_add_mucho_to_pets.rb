class AddMuchoToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :attack, :integer
    add_column :pets, :defense, :integer
    add_column :pets, :curr_hp, :integer
    add_column :pets, :max_hp, :integer
    add_column :pets, :attack_exp, :integer
    add_column :pets, :defense_exp, :integer
    add_column :pets, :hp_exp, :integer
    add_column :pets, :mining_exp, :integer
    add_column :pets, :woodcutting_exp, :integer
  end

  def self.down
    remove_column :pets, :woodcutting_exp
    remove_column :pets, :mining_exp
    remove_column :pets, :hp_exp
    remove_column :pets, :defense_exp
    remove_column :pets, :attack_exp
    remove_column :pets, :max_hp
    remove_column :pets, :curr_hp
    remove_column :pets, :defense
    remove_column :pets, :attack
  end
end
