class AddItemsToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :items, :text
  end

  def self.down
    remove_column :pets, :items
  end
end
