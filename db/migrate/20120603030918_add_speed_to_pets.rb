class AddSpeedToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :speed, :integer
  end

  def self.down
    remove_column :pets, :speed
  end
end
