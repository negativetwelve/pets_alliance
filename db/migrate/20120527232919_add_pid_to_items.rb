class AddPidToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :pid, :integer
  end

  def self.down
    remove_column :items, :pid
  end
end
