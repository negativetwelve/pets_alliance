class RemoveAttackFromItems < ActiveRecord::Migration
  def self.up
    remove_column :items, :attack
    remove_column :items, :defense
    remove_column :items, :hp
    remove_column :items, :energy
    remove_column :items, :money
  end

  def self.down
    add_column :items, :money, :integer
    add_column :items, :energy, :integer
    add_column :items, :hp, :integer
    add_column :items, :defense, :integer
    add_column :items, :attack, :integer
  end
end
