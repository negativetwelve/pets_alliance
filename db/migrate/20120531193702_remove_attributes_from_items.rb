class RemoveAttributesFromItems < ActiveRecord::Migration
  def self.up
    remove_column :items, :attributes
  end

  def self.down
    add_column :items, :attributes, :text
  end
end
