class AddNameToQuests < ActiveRecord::Migration
  def self.up
    add_column :quests, :name, :string
    add_column :quests, :energy, :integer
    add_column :quests, :money, :integer
    add_column :quests, :action, :string
  end

  def self.down
    remove_column :quests, :action
    remove_column :quests, :money
    remove_column :quests, :energy
    remove_column :quests, :name
  end
end
