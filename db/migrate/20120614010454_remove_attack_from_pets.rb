class RemoveAttackFromPets < ActiveRecord::Migration
  def up
    remove_column :pets, :attack
      end

  def down
    add_column :pets, :attack, :integer
  end
end
