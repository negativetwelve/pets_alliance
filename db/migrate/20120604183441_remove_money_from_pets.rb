class RemoveMoneyFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :money
  end

  def self.down
    add_column :pets, :money, :integer
  end
end
