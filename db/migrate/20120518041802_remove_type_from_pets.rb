class RemoveTypeFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :type
  end

  def self.down
    add_column :pets, :type, :string
  end
end
