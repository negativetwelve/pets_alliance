class RemoveExperienceFromPets < ActiveRecord::Migration
  def self.up
    remove_column :pets, :experience
  end

  def self.down
    add_column :pets, :experience, :integer
  end
end
