class AddViewsToCategories < ActiveRecord::Migration
  def self.up
    add_column :categories, :views, :integer
  end

  def self.down
    remove_column :categories, :views
  end
end
