class RemovePetFromItems < ActiveRecord::Migration
  def self.up
    remove_column :items, :pet
  end

  def self.down
    add_column :items, :pet, :references
  end
end
