class AddAllianceCodeToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :alliance_code, :string
  end

  def self.down
    remove_column :users, :alliance_code
  end
end
