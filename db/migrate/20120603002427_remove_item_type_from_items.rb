class RemoveItemTypeFromItems < ActiveRecord::Migration
  def self.up
    remove_column :items, :item_type
  end

  def self.down
    add_column :items, :item_type, :string
  end
end
