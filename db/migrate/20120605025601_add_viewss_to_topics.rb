class AddViewssToTopics < ActiveRecord::Migration
  def self.up
    add_column :topics, :views, :text
  end

  def self.down
    remove_column :topics, :views
  end
end
