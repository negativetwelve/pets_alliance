class AddMaxEnergyToPets < ActiveRecord::Migration
  def self.up
    add_column :pets, :max_energy, :integer
  end

  def self.down
    remove_column :pets, :max_energy
  end
end
