# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

@categories = [["category 1", "this is a category"], ["category 2", "this is another category"], ["test category", "this is the last sample category"]]

@items = [
  ["Short Sword", "weapons", 1, 100, "", { "attack" => 3 }, {}, true, true, 1],
  ["Tee Shirt", "body_gear", 1, 50, "", { "defense" => 1, "max_hp" => 3 }, {}, true, true, 1],
  ["Leather Armor", "body_gear", 3, 200, "", { "defense" => 3, "max_hp" => 5 }, {}, true, true, 1],
  ["Health Potion", "consumables", 1, 25, "Restores 25 health!", {}, { "curr_hp" => 25 }, true, true, 1],
  ["Flimsy Loincloth", "leg_gear", 1, 25, "", { "defense" => 1, "max_hp" => 1 }, {}, true, true, 1],
  ["Silver Sword", "weapons", 2, 200, "", { "attack" => 5 }, {}, true, true, 1],
  ["Katana", "weapons", 5, 500, "", { "attack" => 10 }, {}, true, true, 1],
  ["Bronze Breastplate", "body_gear", 5, 800, "", { "defense" => 6, "max_hp" => 20 }, {}, true, true, 1],
  ["Bronze Helmet", "head_gear", 3, 400, "", { "defense" => 3, "max_hp" => 10 }, {}, true, true, 1]
]

@quests = [["Save the Princess", [1, 0, 1], [25]]]

@locations = [["First Town", {"north" => "Second Town"}, {"shop" => ["First Town Show", 0, 100, 200, 300]}], ["Second Town", {"south" => "First Town"}]]

Location.delete_all
@locations.each_with_index do |location, index|
  Location.create(:pid => index, :name => location[0], :neighbors => location[1], :places => location[2])
end

Category.delete_all
@categories.each_with_index do |category, index|
  Category.create(:pid => index, :title => category[0], :url => category[0].downcase.gsub(" ", "-"), :description => category[1])
end

Item.delete_all
item_pid = 0
@items.each do |item|
  if ["weapons", "body_gear", "leg_gear", "gloves", "shoes", "head_gear", "accessories"].include? item[1]
    Item.create(:pid => item_pid, :name => item[0], :item_type => item[1], :unlock_level => item[2], :cost => item[3].to_s, :description => [], :attributes_list => item[5], :power_ups => item[6], :battle_useable => item[7], :sellable => item[8], :level => 1, :can_equip => true)
    item_pid += 1
    cost = item[3]
    start_values = []
    item[5].keys.each do |j|
        start_values.push(item[5][j])
    end
    start_values_power_ups = []
    item[6].keys.each do |j|
        start_values_powers_ups.push(item[6][j])
    end
    (2..100).each_with_index do |i, index|
      level = index + 2
      new_attributes = {}
      item[5].keys.each_with_index do |k, index|
        value = ((level * 0.25) + start_values[index]*1.1 + 1)
        new_attributes[k] = value.round(3-(value.to_i.to_s.length)).to_i
        start_values[index] = value
      end
      new_power_ups = {}
      item[6].keys.each_with_index do |k, index|
        value_power = ((level * 0.25) + start_values_power_ups[index]*1.1 + 1)
        new_power_ups[k] = value_power.round(3-(value_power.to_i.to_s.length)).to_i
        start_values_power_ups[index] = value_power
      end
      cost = (cost * 1.35)
      Item.create(:pid => item_pid, :name => item[0], :item_type => item[1], :unlock_level => item[2], :cost => cost.round(2-(cost.to_i.to_s.length)).to_s, :description => [], :attributes_list => new_attributes, :power_ups => new_power_ups, :battle_useable => item[7], :sellable => item[8], :level => level, :can_equip => true)
      item_pid += 1
    end
  else
    Item.create(:pid => item_pid, :name => item[0], :item_type => item[1], :unlock_level => item[2], :cost => item[3].to_s, :description => [item[4]], :attributes_list => item[5], :power_ups => item[6], :battle_useable => item[7], :sellable => item[8], :level => 1, :can_equip => false)
    item_pid += 1
  end
end

Quest.delete_all
@quests.each_with_index do |quest, index|
    Quest.create(:pid => index, :name => quest[0], :requirements => { :energy => quest[1][0], :money => quest[1][1], :level => quest[1][2] }, :action => { :money => quest[2][0] })
end