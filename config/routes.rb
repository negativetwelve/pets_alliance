PetsAlliance::Application.routes.draw do

  scope "forums/:url" do
    resources :topics
  end
  
  scope "forums/:url/topics/:topic_id" do
    resources :posts
  end
    
  resources :pets
  resources :users
  resources :quests
    
  resources :sessions,      :only => [:new, :create, :destroy]
  
  match '/worldmap', :to => 'locations#index'
  match '/explore', :to => 'locations#explore'
  match '/worldmap/setlocation', :to => 'pets#set_current_location'
  
  resources :friendships
  match 'alliance/req', :to => 'friendships#req'
  match 'alliance/accept/:id', :to => 'friendships#accept'
  match 'alliance/reject/:id', :to => 'friendships#reject'
  
  root :to => "pages#home"

  match '/contact', :to => 'pages#contact'
  match '/about',   :to => 'pages#about'
  match '/help',    :to => 'pages#help'
  match '/signup',  :to => 'users#new'
  match '/signin',  :to => 'sessions#new'
  match '/signout', :to => 'sessions#destroy'
  match '/leaderboard', :to => 'pages#leaderboard'
  match '/alliance', :to => 'pages#alliance'
  
  match '/forums', :to => 'pages#forums'
  match '/forums/:url', :to => 'categories#show'
  
  match 'battle', :to => 'pets#battle_index'
  match 'battle/:opponent_pet_id/attack', :to => 'battles#attack'
  match 'battle/run_away', :to => 'battles#run_away'
  match 'battle/:id', :to => 'battles#_show'

  match 'pets/:id/select', :to => 'pets#set_current'
  
  match 'quests/:id/job', :to => 'quests#job'
  
  match 'shop', :to => 'items#_index'
  match 'shop/:item_type', :to => 'items#show_by_type'
  match 'shop/:pid/buy', :to => 'items#buy'
  match 'shop/:pid/sell', :to => 'items#sell'
  
  match 'inventory', :to => 'items#inventory'
  match 'inventory/:pid/equip', :to => 'items#equip'
  match 'inventory/:pid/level', :to => 'items#level_up'
  match 'unequip/:position', :to => 'items#unequip'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get :short
  #       post :toggle
  #     end
  #
  #     collection do
  #       get :sold
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get :recent, :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
