class PetsController < ApplicationController
  before_filter :authorized_user, :only => :destroy
  
  PET_TYPES = {
   'Please select' => "",
   'Dragon' => 'Dragon',
   'Cat' => 'Cat',
   }
  
  EXP_LEVELS = {}
  var = 100
  i = 1
  while i <= 1000 do
    EXP_LEVELS[i] = var.round
    var = var + 100 + i * 50
    i += 1
  end
  
  def index
    @pets = Pet.paginate(:page => params[:page])
    @title = "All pets"
  end
  
  def show
    @pet = Pet.find(params[:id])
    @title = @pet.name
  end
  
  def new
    @pet  = Pet.new
    @title = "Create a pet"
  end
  
  def edit
    @title = "Edit pet"
  end
  
  def create
    @pet = current_user.pets.build(params[:pet])
    if @pet.pet_type.empty?
      flash.now[:error] = "Invalid pet type."
      render 'new'
    elsif @pet.save
      redirect_to root_path, :flash => { :success => "Pet created!" }
    else
      render 'new'
    end
  end
  
  def update
    if @pet.update_attributes(params[:pet])
      redirect_to @pet, :flash => { :success => "Pet updated." }
    else
      @title = "Edit pet"
      render 'edit'
    end
  end

  def destroy
    @pet.destroy
    redirect_to root_path, :flash => { :success => "Pet destroyed." }
  end
  
  def set_current
    @pet = Pet.find(params[:id])
    if @pet != current_pet
      current_pet.current = false
      @pet.current = true
      if current_pet.save && @pet.save
        redirect_to root_path, :flash => { :success => "#{@pet.name} is now your active pet!" }
      end
    else
      redirect_to current_user, :flash => { :error => "#{@pet.name} is already your active pet!" }
    end
  end
  
  def set_current_location
    @location = Location.where(:name => params[:location]).first
    @pet = current_pet
    if @pet.current_location.pid == @location.pid
      redirect_to '/worldmap', :flash => { :notice => "You are currently in #{@pet.current_location.name}." }
    elsif @pet.location_history[@location.pid] == false || @pet.location_history[@location.pid] == nil
      redirect_to '/worldmap', :flash => { :error => "You have not yet unlocked #{@location.name}." }
    elsif !@pet.current_location.neighbors.values.include?(@location.name)
      redirect_to '/worldmap', :flash => { :error => "#{@location.name} does not border #{@pet.current_location.name}. You can only travel to neighboring locations" }
    else
      @pet.location = @location
      if @pet.save
        redirect_to '/worldmap', :flash => { :success => "Your current location is now #{@pet.current_location.name}." }
      end
    end
  end
  
  def battle_index
    @pets = Pet.all
    @title = "Battle"
  end
  
  def battle
    @pet = current_pet
    @pet.opponent_id = params[:id]
    if @pet.id == @pet.opponent_id
      redirect_to '/battle', :flash => { :error => "You cannot attack yourself." }
    else
      @battle = Battle.new
      @battle.pet_id = @pet.id
      @battle.opponent_id = @opponent.id
      @pet.save
      @title = "Battle"
      redirect_to '/battle'
    end
  end

  private
  
    def authorized_user
      @pet = current_user.pets.find_by_id(params[:id])
      redirect_to @pet.user if @pet.nil?
    end
end
