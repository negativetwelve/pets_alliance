class PostsController < ApplicationController

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    @topic = Topic.find(params[:topic_id])
    @post = @topic.posts.build(params[:post])
    @post.user_id = current_user.id
    if !@post.content.blank?
      @post.save
      redirect_to :controller => 'topics', :action => 'show', :id => @topic.id
    else
      redirect_to({:controller => 'topics', :action => 'show', :id => @topic.id}, :flash => { :error => "Your post must contain at least one character." })
    end
  end
  
  def update
    @topic = Topic.find(params[:topic_id])
    @post = Post.find(params[:id])
    if @post.update_attributes(params[:post])
      redirect_to({:controller => 'topics', :action => 'show', :id => @topic.id}, :flash => { :success => "Post updated." })
    else
      @title = "Edit post"
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
  end
  
end
