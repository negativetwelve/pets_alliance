class TopicsController < ApplicationController
  skip_before_filter :login_required, :only => :show
  
  def show
    @topic = Topic.find(params[:id])
    @posts = @topic.posts.paginate(:page => params[:page], :per_page => 10)
    @topic.update_column(:views, (@topic.views.to_i + 1).to_s)
  end
  
  def edit
    @topic = Topic.find(params[:id])
  end

  def new
    @category = Category.where(:url => params[:url]).first
    @topic = Topic.new
    @post = Post.new
  end

  def create  
    @category = Category.where(:url => params[:url]).first
    @topic = @category.topics.build(params[:topic])
    if !@topic.title.blank? && @topic.save
      @post = Post.new(:content => params[:post][:content], :topic_id => @topic.id, :user_id => current_user.id)      
      if !@post.content.blank? && @post.save
        flash[:notice] = "Successfully created topic."  
        redirect_to :controller => 'topics', :action => 'show', :id => @topic.id
      else
        @topic.destroy
        redirect_to({:controller => 'topics', :action => 'new'}, :flash =>  { :error => "Your post must contain at least one character."})
      end  
    else
      redirect_to({:controller => 'topics', :action => 'new'}, :flash => { :error => "Your topic is missing a title."})
    end  
  end

end
