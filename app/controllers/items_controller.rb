class ItemsController < ApplicationController

  ITEM_QUANTITIES = (1..100).to_a
  CATEGORIES = ["consumables", "weapons", "body_gear", "leg_gear", "gloves", "shoes", "head_gear", "accessories"]
  EQUIPMENT_ALLOWED = {"weapons" => ["weapon1", "weapon2"], "body_gear" => ["body"], "leg_gear" => ["legs"], "gloves" => ["hands"], "shoes" => ["feet"], "head_gear" => ["head"], "accessories" => ["accessory1", "accessory2", "accessory3"]}
  EQUIPMENT_SLOTS = {"weapon1" => "weapon", "weapon2" => "weapon", "body" => "body_gear", "legs" => "leg_gear", "hands" => "gloves", "feet" => "shoes", "head" => "head_gear", "accessory1" => "accessories", "accessory2" => "accessories", "accessory3" => "accessories"}
  
  def buy
    @pet = current_pet
    @item = Item.where( :pid => params[:pid] ).first
    @quantity = params[:quantity].to_i
    if @quantity == 0
      redirect_to '/shop/' + @item.item_type.to_s, :flash => { :error => "Sorry, this is an invalid action." }
    elsif @item.cost.to_i * @quantity > @pet.money.to_i
      redirect_to '/shop/' + @item.item_type.to_s, :flash => { :error => "You do not have enough money! Your pet needs $#{@item.cost.to_i * @quantity - @pet.money.to_i} more to buy #{@quantity} #{@item.name}(s)." }
    else
      if @pet.items_list[params[:pid]] == nil
        @pet.items_list[params[:pid]] = @quantity
      else
        @pet.items_list[params[:pid]] += @quantity
      end
      if @pet.items_list["total_bought"] == nil
        @pet.items_list["total_bought"] = @quantity
      else
        @pet.items_list["total_bought"] += @quantity
      end
      @pet.money = (@pet.money.to_i - @item.cost.to_i * @quantity).to_s
      if @pet.save
        redirect_to '/shop/' + @item.item_type.to_s, :flash => { :success => "You have bought #{@quantity} #{@item.name} and spent $#{@item.cost.to_i * @quantity}." }
      else
        redirect_to '/shop/' + @item.item_type.to_s, :flash => { :error => "An error has occurred please contact us." }
      end
    end
  end
  
  def sell
    @pet = current_pet
    @item = Item.where( :pid => params[:pid] ).first
    @quantity = params[:quantity].to_i
    count = 0
    @pet.equipment.values.each do |i|
      if i == params[:pid].to_i
        count += 1
      end
    end
    if @quantity > @pet.items_list[params[:pid]].to_i
      redirect_to :back, :flash => { :error => "You do not have #{@quantity} #{@item.name}(s)!" }
    elsif @quantity > (@pet.items_list[params[:pid]].to_i - count)
      redirect_to :back, :flash => { :error => "You need to unequip your item before you can sell it!" }
    else
      @pet.items_list[params[:pid]] -= @quantity
      gained = @item.cost.to_i * @quantity * 0.5
      @pet.money = (@pet.money.to_i + gained).to_s
      if @pet.items_list["total_sold"] == nil
        @pet.items_list["total_sold"] = @quantity
      else
        @pet.items_list["total_sold"] += @quantity
      end
      if @pet.save
        redirect_to :back, :flash => { :success => "You have sold #{@quantity} #{@item.name} and gained $#{gained.to_i}." }
      else
        redirect_to :back, :flash => { :error => "An error has occurred please contact us." }
      end
    end
  end
  
  def show_by_type
    @categories = CATEGORIES
    @items = Item.where( :item_type => params[:item_type], :level => 1)
    @title = params[:item_type].titleize
  end

  def _index
    @categories = CATEGORIES
    @title = "Shop"
  end
  
  def inventory
    @categories = CATEGORIES
    @items_list = current_pet.items_list
    @title = "Inventory"
  end
  
  def equip
    @item = Item.where(:pid => params[:pid]).first
    if !EQUIPMENT_ALLOWED.keys.include? @item.item_type
      redirect_to inventory_path, :flash => { :error => "You cannot equip #{@item.name}(s)!" }
    else
    @pet = current_pet
    bool = false
    EQUIPMENT_ALLOWED[@item.item_type.to_s].each do |i|
      bool = ((@pet.equipment[i] == nil) || bool)
      if !bool && EQUIPMENT_ALLOWED[@item.item_type.to_s].last == i
        redirect_to inventory_path, :flash => { :error => "#{@pet.name} already has the #{@item.item_type.titleize.gsub("_", " ")} spot(s) filled. Unequip an item below." }
        break
      end
    end
    EQUIPMENT_ALLOWED[@item.item_type.to_s].each do |i|
      @count = 0
      if @pet.equipment[i] == nil
        EQUIPMENT_ALLOWED[@item.item_type.to_s].each do |j|
          if @pet.equipment[j] == params[:pid].to_i
            @count += 1
          end
        end
        if @count >= @pet.items_list[params[:pid]]
          redirect_to inventory_path, :flash => { :error => "#{@pet.name} only has #{@pet.items_list[params[:pid]]} #{Item.where(:pid => params[:pid]).first.name}(s)." }
          break
        else
          @pet.equipment[i] = params[:pid].to_i
          @item = Item.where(:pid => @pet.equipment[i]).first
          @item.attributes_list.keys.each do |o|
            @pet.petstats[o] += @item.attributes_list[o]
          end
          @pet.save
          redirect_to inventory_path, :flash => { :success => "#{@pet.name} has just equipped #{@item.name}!" }
          break
        end
      end
    end
    end
  end
  
  def unequip
    @pet = current_pet
    @item = Item.where(:pid => @pet.equipment[params[:position]]).first
    if @pet.equipment[params[:position]] == nil
      redirect_to inventory_path, :flash => { :error => "You do not have an item to unequip." }
    else
      @pet.equipment[params[:position]] = nil
      @item.attributes_list.keys.each do |o|
        @pet.petstats[o] -= @item.attributes_list[o]
      end
      if @pet.petstats['curr_hp'] < @pet.petstats['max_hp']
        @pet.petstats['curr_hp'] = @pet.petstats['max_hp']
      end
      @pet.save
      redirect_to inventory_path, :flash => { :notice => "#{@pet.name} has just unequipped #{@item.name}." }
    end  
  end
  
  def level_up
    @pet = current_pet
    @item = Item.where(:pid => params[:pid]).first
    @num = 1
    if @pet.petstats['item_points'] < @item.level
      redirect_to inventory_path, :flash => { :error => "#{@pet.name} does not have enough item skill points. You need #{@item.level} points to increase your #{@item.name}'s level."}
    elsif @pet.items_list[params[:pid]].to_i <= 0
      redirect_to inventory_path, :flash => { :error => "You do not have anymore level #{@item.level} #{@item.name}(s)."}
    elsif @num != 1
      redirect_to inventory_path, :flash => { :error => "Sorry this is an invalid action."}
    else      
      @pet.items_list[params[:pid].to_i.to_s] -= 1
      num = params[:pid].to_i
      @pet.petstats['item_points'] -= @item.level
      positions = EQUIPMENT_ALLOWED[@item.item_type]
      positions.each do |i|
        if @pet.equipment[i] == params[:pid].to_i
          @pet.equipment[i] += @num
          @item.attributes_list.keys.each do |o|
            @pet.petstats[o] -= @item.attributes_list[o]
          end
          @new_item = Item.where(:pid => num+1).first
          @new_item.attributes_list.keys.each do |o|
            @pet.petstats[o] += @new_item.attributes_list[o]
          end
          break
        end
      end
      if @pet.items_list[(num+@num).to_s] == nil
        @pet.items_list[(num+@num).to_s] = 1
      else
        @pet.items_list[(num+@num).to_s] += 1
      end
      @pet.save
      redirect_to inventory_path, :flash => { :success => "You just leveled up your #{@item.name}!" }
    end
  end
    
end