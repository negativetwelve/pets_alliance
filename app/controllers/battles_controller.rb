class BattlesController < ApplicationController

  def _show
    @pet = current_pet
    if current_pet.in_battle?
    elsif current_pet.stat('curr_hp') > 0
    @temp = Pet.find(params[:id])
    @opponent = Opponent.new(:name => @temp.name, :attack => @temp.stat('attack'), :defense => @temp.stat('defense'), :hp => @temp.stat('max_hp'), :equipment => @temp.equipment, :opp_id => params[:id], :pet_id => @pet.id, :alliance_count => @temp.user.friends.count.to_i)
    if @opponent.nil?
      flash[:error] = "An error has occurred."
    elsif @opponent.opp_id == @pet.id
      redirect_to '/battle', :flash => { :error => "You cannot attack yourself." }
    else
      if Battle.start(@pet, @opponent)
        @opponent.save
        flash[:notice] = "You have started a battle!"
        redirect_to '/battle'
      else
        flash[:error] = "An error has occurred."
      end
    end
    else
      redirect_to '/battle', :flash => { :error => "You do not have any HP, heal first!" }
    end
  end

  def attack 
    # Pull the monster we were fighting from the session.
    if params[:token].nil?
      redirect_to '/battle', :flash => { :error => "Invalid action." }
    else
    @pet = current_pet
    @opponent = Opponent.where(:pet_id => @pet.id).last
    if @opponent.nil?
      redirect_to '/battle', :flash => { :error => "You must select an opponent for battle." }
    else
      @battle = Battle.where(:pet_id => @pet.id, :opponent_id => @opponent.id).last
 
    # Fight the monster and get the transcript of the fight.
      @notice = @pet.fight(@opponent, @battle)
      if !@notice.nil?
        flash[:notice] = @notice
      else
        
      end 
      redirect_to '/battle'
    end
    end
  end
  
  def run_away
    @opponent = Opponent.where(:pet_id => current_pet.id).last
    @pet = current_pet
    @battle = Battle.where(:pet_id => @pet.id, :opponent_id => @opponent.id).last
    @battle.destroy
    @opponent.destroy
    @pet.runs += 1
    @pet.in_battle = false
    @pet.save
 
    redirect_to({:controller => "pages", :action => "home"}, :notice => "You successfully ran away.")
  end

end
