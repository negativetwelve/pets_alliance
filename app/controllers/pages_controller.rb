class PagesController < ApplicationController
  skip_before_filter :login_required


  def home
    @title = "Pets Alliance"
    if signed_in?
    end
  end

  def contact
    @title = "Contact"
  end
  
  def about
    @title = "About"
  end
  
  def help
    @title = "Help"
  end
  
  def leaderboard
    @pets = Pet
    @title = "Leaderboard"
  end
  
  def forums
    @categories = Category.all
    @title = "Official Forums of Pets Alliance"
  end
  
  def alliance
    @title = "Alliance"
    @users = User.all
    @user = current_user
  end
  
end
