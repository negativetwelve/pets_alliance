class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper
  before_filter :login_required
  
  private
  
    def login_required
      if current_user.nil?
        deny_access
      end
    end
end