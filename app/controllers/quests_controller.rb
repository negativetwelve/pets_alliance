class QuestsController < ApplicationController

  def index
    @quests = Quest.paginate(:page => params[:page])
    @title = "All quests"
  end
  
  def job
    @quest = Quest.find(params[:id])
    @pet = current_pet
    energy = @quest.requirements[:energy].to_i
    money = @quest.requirements[:money].to_i
    level = @quest.requirements[:level].to_i
    money_gained = @quest.action[:money].to_i
    if @pet.curr_energy < energy
      redirect_to quests_path, :flash => { :error => "Your pet does not have enough energy to do this quest!" }
    elsif @pet.money.to_i < money
      redirect_to quests_path, :flash => { :error => "Your pet does not have enough money to do this quest!" }
    elsif @pet.overall_lvl < level
      redirect_to quests_path, :flash => { :error => "Your pet does not have a high enough level to do this quest!" }
    else
      @pet.curr_energy -= energy
      @pet.money = (@pet.money.to_i - money).to_s
      @pet.money = (@pet.money.to_i + money_gained).to_s
      if @pet.save
        redirect_to quests_path, :flash => { :success => "Quest completed! You have spent $#{money} and used #{energy} energy. You have also gained $#{money_gained}." }
      else
        redirect_to quest, :flash => { :error => "An error has occurred please contact us." }
      end
    end
  end
  
end
