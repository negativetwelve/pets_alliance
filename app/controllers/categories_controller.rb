class CategoriesController < ApplicationController
  skip_before_filter :login_required, :only => :show

  def show
    @category = Category.where(:url => params[:url]).first
    @topics = @category.topics.order("updated_at DESC").paginate(:page => params[:page], :per_page => 30)
  end

end
