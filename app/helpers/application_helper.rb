module ApplicationHelper
  
  # Return a title on a per-page basis.
  def title
    base_title = "Pets Alliance"
    if @title.nil?
      base_title
    else
      "#{@title}"
    end
  end
  
  def logo
    image_tag('logo.png', :alt => "Pets Alliance", :class => "round")
  end
  
  def pet_image(pet, size)
    image_tag("pets/#{pet.pet_type.downcase}.png", :alt => "#{pet.pet_type}", :class => "info", :size => "#{size}x#{size}")
  end
  
  def item_image(item, size)
    image_tag("items/#{item.name.downcase.gsub(" ", "_")}.png", :alt => "#{item.name}", :size => "#{size}x#{size}")
  end
end
