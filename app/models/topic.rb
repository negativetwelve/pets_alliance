class Topic < ActiveRecord::Base
  belongs_to :category
  has_many :posts, :dependent => :destroy
  
  before_create :setup

  def setup
    self.views = 0
  end
end

