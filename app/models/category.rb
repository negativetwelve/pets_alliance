class Category < ActiveRecord::Base
  has_many :topics, :dependent => :destroy
  has_many :posts, :through => :topics
  before_create :setup
  
  def setup
    self.id = self.pid
  end
  
end
