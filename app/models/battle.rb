class Battle < ActiveRecord::Base
  belongs_to :pet
  belongs_to :opponent
  
  serialize :log, Array

  def self.start(pet, opponent)
    return false if pet == opponent
    f1 = new(:pet => pet, :opponent => opponent, :in_progress => true)
    pet.in_battle = true
    transaction do
      f1.save
      pet.save
    end
    return true
  end

end
