class Opponent < ActiveRecord::Base
  belongs_to :pet
  serialize :equipment
  
  def alive?
    self.hp > 0
  end
  
end
