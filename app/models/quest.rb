class Quest < ActiveRecord::Base
  attr_accessible :name, :action, :requirements, :pid
  serialize :action, Hash
  serialize :requirements, Hash
end
