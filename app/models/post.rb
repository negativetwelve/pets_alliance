class Post < ActiveRecord::Base
  belongs_to :topic
  belongs_to :user
  
  after_save do
    topic.update_attribute(:updated_at, Time.now)
  end
end
