class Item < ActiveRecord::Base  
  attr_accessible :pid, :name, :item_type, :unlock_level, :cost, :description, :attributes_list, :power_ups, :battle_useable, :sellable, :level, :can_equip
  before_create :setup_description
  
  serialize :description, Array

  serialize :attributes_list, Hash
  serialize :power_ups, Hash
  
  def setup_description
    description = []
    self.attributes_list.keys.each do |i|
    if self.attributes_list[i] > 0
      description.push("+#{self.attributes_list[i]} #{i.to_s.titleize}")
    end
    if self.attributes_list[i] < 0
      description.push("#{self.attributes_list[i]} #{i.to_s.titleize}")
    end
    self.description = description
  end
  end
  
end