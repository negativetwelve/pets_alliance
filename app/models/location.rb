class Location < ActiveRecord::Base
  has_many :pets
  serialize :neighbors, Hash
  serialize :places, Hash
end
