class Pet < ActiveRecord::Base
  before_create :setup
  
  belongs_to :user
  belongs_to :location
  
  has_one :battle
  has_one :opponent  
  
  validates :name,  :presence => true,
                    :length   => { :within => 2..20},
                    :uniqueness => { :case_sensitive => false }
                      
  serialize :items_list, Hash
  serialize :equipment, Hash
  serialize :petstats, Hash
  serialize :location_history, Hash
  serialize :log, Array
  serialize :toggles, Hash
  
  CHECKED_VARIABLES = { 'level' => 1, 'experience' => 0, 'skill_points' => 0, 'item_points' => 0, 'curr_energy' => 10, 'max_energy' => 10, 'curr_hp' => 40, 'max_hp' => 40, 'mining_lvl' => 1, 'woodcutting_lvl' => 1, 'mining_exp' => 0, 'woodcutting_exp' => 0, 'base_attack' => 10, 'base_defense' => 10, 'attack' => 10, 'defense' => 10 }
  
  PET_STATS = { 'Dragon' => { 'curr_hp' => 20, 'max_hp' => 20, 'base_attack' => 5, 'base_defense' => 5, 'attack' => 5, 'defense' => 5 }, 'Cat' => { 'curr_hp' => -5, 'max_hp' => -5, 'base_attack' => 3, 'base_defense' => -2, 'attack' => 3, 'defense' => -2 } }
  
  PET_STAT_MODIFIERS = { 'Dragon' => { 'max_hp' => 5, 'base_attack' => 1, 'base_defense' => 1, 'attack' => 1, 'defense' => 1 }, 'Cat' => { 'max_hp' => 5, 'base_attack' => 1, 'base_defense' => 1, 'attack' => 1, 'defense' => 1 } }
  
  def stat(n)
    if self.petstats[n] == nil && CHECKED_VARIABLES.keys.include?(n)
      self.petstats[n] = CHECKED_VARIABLES[n]
      self.save
    end
    if self.petstats[n] != nil
      self.petstats[n]
    end
  end
  
  def current_location
    if self.location == nil
      self.location = Location.where(:pid => 0).first
      self.save
    end
    self.location
  end
  
  def grow_level(pet_type, level)
    log = ["level", level, []]
    PET_STAT_MODIFIERS[pet_type].keys.each do |i|
      self.petstats[i] += PET_STAT_MODIFIERS[pet_type][i]
      log[2].append([i, PET_STAT_MODIFIERS[pet_type][i]])
    end
    self.log.append(log)
    self.save
  end
  
  def gain_experience(experience, level)
    next_level_exp = PetsController::EXP_LEVELS[level]
    if self.petstats['experience'] + experience < next_level_exp
      difference = level - self.petstats['level']
      self.petstats['level'] = level
      self.petstats['experience'] += experience
      (1..difference).each_with_index do |i, index|
        self.grow_level(self.pet_type, level-(difference-index)+1)
      end
      self.save
    else
      self.gain_experience(experience, level+1)
    end
  end
                   
  def setup
    if self.user.pets.count > 0
      self.current = false
    else
      self.current = true
    end
    
    if self.location == nil
      self.location = Location.where(:pid => 0).first
    end
    
    self.in_battle = false

    self.items_list = {}
    self.equipment = {}
    self.petstats = {}
    self.location_history = {}
    self.log = []
    self.toggles = {}
 
    Location.all.each_with_index do |location, index|
      self.location_history[location.pid] = false
    end
    
    self.location_history[0] = true
        
    self.win = 0
    self.loss = 0
    self.idle_wins = 0
    self.idle_losses = 0
    self.runs = 0
    
    self.money = 0
    
    CHECKED_VARIABLES.keys.each do |i|
      if self.petstats[i] == nil
        self.petstats[i] = CHECKED_VARIABLES[i]
      end
    end
    
    PET_STATS.keys.each do |i|
      if self.pet_type == i
        PET_STATS[i].keys.each do |k|
          self.petstats[k] += PET_STATS[i][k]
        end
      end
    end
    
  end
  
  def calculate(stat, num)
    self.petstats[stat] += num
    self.save
  end
  
  def alive?
    self.petstats['curr_hp'] > 0
  end
 
  def fight(opponent, battle)
    if battle.log == nil
      battle.log = Array.new
    end
    
    if battle.turn_number == nil
      battle.turn_number = 0
    end
 
    # This is a small workaround for the fact that we aren't getting the user's name yet
    # when they signup. All we have for the user is the login.
    #if (self.name.empty?)
     # self.name = self.login
    #end
 
    if (self.alive? and opponent.alive?)
      battle.turn_number += 1
      
      if battle.turn_number == 1
        if self.user.friends.count.to_i >= opponent.alliance_count
          battle.first = true
          attacker = self
          defender = opponent
        else
          battle.first = false
          attacker = opponent
          defender = self
        end
      elsif (battle.turn_number != 1 && battle.first)
        battle.first = false
        attacker = opponent
        defender = self
      elsif (battle.turn_number != 1 && !battle.first)
        battle.first = true
        attacker = self
        defender = opponent
      end
 
      if (true)
        damage = rand(20)+5
 
        # We allow damage to take you below zero hitpoints. Presumably the funeral is
        # closed casket in those cases.
        if defender.class == Opponent
          defender.hp -= damage
        else
          defender.petstats['curr_hp'] -= damage
        end
        
        if !defender.alive?
          turn = Hash.new
          turn["winner"] = attacker.name
          turn["loser"] = defender.name
          battle.in_progress = false
        else
        # We'll only make a turn record for those cases where something actually happens.
        turn = Hash.new
        turn["attacker"] = attacker.name
        turn["defender"] = defender.name
        turn["damage"] = damage
        end
        battle.log << turn
        # Save this turn in the combat transcript.
        battle.save
      end
    end
 
    # Somebody is dead, let's see who and take appropriate action.
    if (self.alive? && !battle.in_progress?)
      # Yay, you lived. You get gold.
      self.money = (self.money.to_i + 100).to_s
      self.in_battle = false
      self.win += 1
      pet = Pet.find(self.opponent.opp_id)
      self.log.append(["battle", "win", self.id, pet.id, "$", "experience"])
      level = self.petstats['level']
      self.gain_experience(1000, self.petstats['level'])
      diff = self.petstats['level'] - level
      if diff > 0
        @log = "#{self.name} grew #{diff} level(s)."
      end
      result = ["beat", pet.name]
      pet.idle_losses += 1
      pet.log.append(["battle", "idle loss", pet.id, self.id, "$"])
      pet.save
      opponent.destroy
      battle.destroy
    elsif (!self.alive? && !battle.in_progress?)
      # You lost so these things happen
      self.petstats['curr_hp'] = 0
      self.in_battle = false
      self.loss += 1
      pet = Pet.find(self.opponent.opp_id)
      self.log.append(["battle", "loss", self.id, pet.id, "$"])
      result = ["lost to", pet.name]
      pet.idle_wins += 1
      pet.log.append(["battle", "idle wins", pet.id, self.id, "$"])
      pet.save
      opponent.destroy
      battle.destroy
      @log = ""
    else
      opponent.save
    end
 
    self.save
    
    # Return the results of combat.
    if turn['attacker'].nil?
      "You #{result[0]} #{result[1]}! " + @log
    else
      "#{turn['attacker']} hit #{turn['defender']} for #{turn['damage']} damage."
    end
  end
  
end