require 'spec_helper'

describe "items/new.html.erb" do
  before(:each) do
    assign(:item, stub_model(Item,
      :name => "MyString",
      :item_type => "MyString",
      :unlock_level => 1,
      :cost => 1,
      :attack => 1,
      :defense => 1,
      :hp => 1,
      :energy => 1,
      :money => 1,
      :pet => nil
    ).as_new_record)
  end

  it "renders new item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => items_path, :method => "post" do
      assert_select "input#item_name", :name => "item[name]"
      assert_select "input#item_item_type", :name => "item[item_type]"
      assert_select "input#item_unlock_level", :name => "item[unlock_level]"
      assert_select "input#item_cost", :name => "item[cost]"
      assert_select "input#item_attack", :name => "item[attack]"
      assert_select "input#item_defense", :name => "item[defense]"
      assert_select "input#item_hp", :name => "item[hp]"
      assert_select "input#item_energy", :name => "item[energy]"
      assert_select "input#item_money", :name => "item[money]"
      assert_select "input#item_pet", :name => "item[pet]"
    end
  end
end
