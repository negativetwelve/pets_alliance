require 'spec_helper'

describe "pets/index.html.erb" do
  before(:each) do
    assign(:pets, [
      stub_model(Pet,
        :name => "Name",
        :pet_type => "Pet Type",
        :user => nil,
        :description => "MyText",
        :overall_lvl => 1
      ),
      stub_model(Pet,
        :name => "Name",
        :pet_type => "Pet Type",
        :user => nil,
        :description => "MyText",
        :overall_lvl => 1
      )
    ])
  end

  it "renders a list of pets" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Pet Type".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
