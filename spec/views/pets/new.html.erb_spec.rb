require 'spec_helper'

describe "pets/new.html.erb" do
  before(:each) do
    assign(:pet, stub_model(Pet,
      :name => "MyString",
      :pet_type => "MyString",
      :user => nil,
      :description => "MyText",
      :overall_lvl => 1
    ).as_new_record)
  end

  it "renders new pet form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => pets_path, :method => "post" do
      assert_select "input#pet_name", :name => "pet[name]"
      assert_select "input#pet_pet_type", :name => "pet[pet_type]"
      assert_select "input#pet_user", :name => "pet[user]"
      assert_select "textarea#pet_description", :name => "pet[description]"
      assert_select "input#pet_overall_lvl", :name => "pet[overall_lvl]"
    end
  end
end
